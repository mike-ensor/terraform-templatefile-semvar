
# regex("(\\d).(\\d).(\\d)", "x.y.z")

# if x > x1 => true # easy path
# if x==x1 && y > y1 => true
# if x==x1 && y==y1 && z > z1 => true

locals {

  semver_break           = regex("(\\d).(\\d).(\\d)", "${var.request_semver}")
  deviation_semver_break = regex("(\\d).(\\d).(\\d)", "${var.threshhold_semvar}")
  in_major               = "${local.semver_break[0]}"
  in_minor               = "${local.semver_break[1]}"
  in_patch               = "${local.semver_break[2]}"

  dev_major = "${local.deviation_semver_break[0]}"
  dev_minor = "${local.deviation_semver_break[1]}"
  dev_patch = "${local.deviation_semver_break[2]}"

  # If the major number is larger-than, minor and patch don't matter, only if equal
  major_major = "${local.in_major > local.dev_major ? true : false}"
  major       = "${local.in_major >= local.dev_major ? true : false}"
  minor       = "${local.major && (local.in_minor >= local.dev_minor) ? true : false}"
  patch       = "${local.minor && (local.in_patch >= local.dev_patch) ? true : false}"

  is_greater_or_equal = local.major_major ? true : (local.patch ? true : false)

}

data "template_file" "outputfile" {
  template = "${file("${path.module}/template.tpl")}"

  vars = {
    INSTALL_OR_ENABLE_REDIS_LABEL = "${var.redis_install_or_enable_label[local.is_greater_or_equal == false ? 0 : 1]}"
    INSTALL_OR_ENABLE_REDIS_VALUE = "${var.redis_enabled}"
  }
}